use proc_macro::TokenStream;
use quote::quote;
use syn::{self, Expr, ExprLit, Lit, Meta, MetaNameValue};

const EVENT_ID_ATTRIBUTE_NAME: &str = "event_id";

#[proc_macro_derive(GeniusEvent, attributes(event_id))]
pub fn genius_event_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse_macro_input!(input as syn::DeriveInput);

    let name = &ast.ident;
    let generics = ast.generics;
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();

    let id = ast
        .attrs
        .iter()
        .find(|attr| attr.path().is_ident(EVENT_ID_ATTRIBUTE_NAME))
        .expect("Required `event_id` attribute wasn't given");
    let Meta::NameValue(MetaNameValue { ref value, .. }) = id.meta else {
        panic!("`event_id` attribute must be in name-value format")
    };
    let Expr::Lit(ExprLit { lit, .. }) = value else {
        panic!("Right-hand side of `event_id` must be a literal expression")
    };
    let Lit::Str(id) = lit else {
        panic!("Right-hand side of `event_id` must be a string")
    };

    quote! {
        impl #impl_generics ::rust_genius_pdk::event::Event for #name #ty_generics #where_clause {
            const IDENTIFIER: &'static str = #id;
        }
    }
    .into()
}
