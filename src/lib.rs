pub mod event;
mod ffi;
pub mod logger;

pub use extism_pdk as __extism;
pub use log as __log;
use serde::{Deserialize, Serialize};

pub use rust_genius_pdk_macros::*;
use serde_json::Value;

pub mod prelude {
    pub use crate::{event::RawEvent, genius_eventhandler, genius_loadpoint, PluginMetadata};
    pub use extism_pdk::FnResult;
    pub use log::{debug, error, info, warn};
    pub use rust_genius_pdk_macros::GeniusEvent;
}

#[derive(Serialize)]
pub struct PluginMetadata {
    #[serde(rename = "Name")]
    pub name: String,
    #[serde(rename = "Version")]
    pub version: String,
}

#[macro_export]
macro_rules! genius_loadpoint {
    ($load_fn:path) => {
        mod __genius_loadpoint {
            static LOGGER: $crate::logger::GeniusLogger = $crate::logger::GeniusLogger;

            use extism_pdk::*;
            use $crate::__extism as extism_pdk;
            #[plugin_fn]
            pub fn on_load(_: ()) -> FnResult<Json<$crate::PluginMetadata>> {
                // TODO: error handling?
                let _ = $crate::__log::set_logger(&LOGGER)
                    .map(|()| $crate::__log::set_max_level($crate::__log::LevelFilter::Debug));
                $load_fn().map(|metadata| Json(metadata))
            }
        }
    };
}

#[macro_export]
macro_rules! genius_eventhandler {
    ($event_fn:path) => {
        mod __genius_eventhandler {
            use extism_pdk::*;
            use $crate::__extism as extism_pdk;
            #[plugin_fn]
            pub fn on_event(Json(event): Json<$crate::event::RawEvent>) -> FnResult<()> {
                $event_fn(event)
            }
        }
    };
}

#[must_use]
pub fn show_messagebox(title: &str, content: &str) -> Result<(), extism_pdk::Error> {
    unsafe { ffi::show_messagebox(extism_pdk::Json(ffi::MessageboxPayload { title, content })) }
}

#[must_use]
pub fn register_resource(name: &str, value: impl Serialize) -> Result<(), extism_pdk::Error> {
    unsafe {
        ffi::register_resource(extism_pdk::Json(ffi::RegisterResourcePayload {
            name,
            value,
        }))
    }
}

#[must_use]
pub fn get_resource<T>(name: &str) -> Result<T, extism_pdk::Error>
where
    for<'de> T: Deserialize<'de>,
{
    let raw = unsafe { ffi::get_resource(extism_pdk::Json(ffi::GetResourcePayload { name })) }?;
    let t = serde_json::from_str(&raw)?;
    Ok(t)
}

#[must_use]
pub fn register_icon(name: &str, geom: &str) -> Result<(), extism_pdk::Error> {
    unsafe { ffi::register_icon(extism_pdk::Json(ffi::RegisterIconPayload { name, geom })) }
}

#[must_use]
pub fn register_stateful_icon(
    name: &str,
    normal_geom: &str,
    active_geom: &str,
) -> Result<(), extism_pdk::Error> {
    unsafe {
        ffi::register_stateful_icon(extism_pdk::Json(ffi::RegisterStatefulIconPayload {
            name,
            normalGeom: normal_geom,
            activeGeom: active_geom,
        }))
    }
}

#[must_use]
pub fn create_page(
    title: &str,
    path: &str,
    icon_name: &str,
    content_xaml: &str,
) -> Result<(), extism_pdk::Error> {
    unsafe {
        ffi::create_page(extism_pdk::Json(ffi::CreatePagePayload {
            title,
            path,
            iconName: icon_name,
            contentXaml: content_xaml,
        }))
    }
}

#[must_use]
pub fn show_modal(
    content_xaml: &str,
    arg: impl Serialize
) -> Result<Value, extism_pdk::Error> {
    unsafe {
        ffi::show_modal(extism_pdk::Json(ffi::ModalPayload {
            arg,
            contentXaml: content_xaml,
        }))
    }
}

#[must_use]
pub fn dispatch_event(name: &str, arg: impl Serialize) -> Result<(), extism_pdk::Error> {
    unsafe { ffi::dispatch_event(extism_pdk::Json(ffi::DispatchEventPayload { name, arg })) }
}


#[must_use]
pub fn show_notification(content: &str) -> Result<(), extism_pdk::Error> {
    unsafe { ffi::show_notification(extism_pdk::Json(ffi::NotificationPayload { content })) }
}

#[must_use]
pub fn create_widget(
    name: &str,
    icon: &str,
    template: &str,
    edit_template: &str,
) -> Result<(), extism_pdk::Error> {
    unsafe {
        ffi::register_widget(extism_pdk::Json(ffi::RegisterWidgetPayload {
            name,
            icon,
            template,
            editTemplate: edit_template,
        }))
    }
}