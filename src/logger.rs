use log::{Level, Metadata, Record};
use serde::Serialize;

use crate::ffi;

#[derive(Serialize)]
pub enum LogLevel {
    Error,
    Warning,
    Info,
    Debug,
}

pub struct GeniusLogger;

impl log::Log for GeniusLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= Level::Debug
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            let level = match record.metadata().level() {
                Level::Error => LogLevel::Error,
                Level::Warn => LogLevel::Warning,
                Level::Info => LogLevel::Info,
                Level::Debug => LogLevel::Debug,
                _ => return,
            };
            unsafe {
                // TODO: should we handle errors?
                let _ = ffi::genius_log(extism_pdk::Json(ffi::LogPayload {
                    level,
                    message: &format!("{}", record.args()),
                }));
            }
        }
    }

    fn flush(&self) {}
}
