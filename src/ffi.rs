use extism_pdk::*;
use serde::Serialize;

macro_rules! impl_hostfn_with_payload {
    ($name:ident $(-> $ret:ty)?, $payload:ident$(<$l:lifetime>)? $(with $g:ident)? {
        $($fieldn:ident: $fieldty:ty,)*
    }) => {
        #[derive(Serialize)]
        #[allow(non_snake_case)]
        pub(crate) struct $payload<$($l,)? $($g: Serialize,)?> {
            $(
                pub(crate) $fieldn: $fieldty,
            )*
        }

        #[host_fn]
        extern "ExtismHost" {
            pub(crate) fn $name$(<$g: Serialize>)?(payload: Json<$payload$(<$g>)?>) $(-> $ret)?;
        }
    };
}

impl_hostfn_with_payload!(
    show_messagebox,
    MessageboxPayload<'a> {
        title: &'a str,
        content: &'a str,
    }
);

impl_hostfn_with_payload!(
    register_icon,
    RegisterIconPayload<'a> {
        name: &'a str,
        geom: &'a str,
    }
);

impl_hostfn_with_payload!(
    register_stateful_icon,
    RegisterStatefulIconPayload<'a> {
        name: &'a str,
        normalGeom: &'a str,
        activeGeom: &'a str,
    }
);

impl_hostfn_with_payload!(
    register_resource,
    RegisterResourcePayload<'a> with S1 {
        name: &'a str,
        value: S1,
    }
);

impl_hostfn_with_payload!(
    get_resource -> String,
    GetResourcePayload<'a> {
        name: &'a str,
    }
);

impl_hostfn_with_payload!(
    create_page,
    CreatePagePayload<'a> {
        title: &'a str,
        path: &'a str,
        iconName: &'a str,
        contentXaml: &'a str,
    }
);

impl_hostfn_with_payload!(
    dispatch_event,
    DispatchEventPayload<'a> with S1 {
        name: &'a str,
        arg: S1,
    }
);

impl_hostfn_with_payload!(
    genius_log,
    LogPayload<'a> {
        level: crate::logger::LogLevel,
        message: &'a str,
    }
);

impl_hostfn_with_payload!(
    show_notification,
    NotificationPayload<'a> {
        content: &'a str,
    }
);

impl_hostfn_with_payload!(
    register_widget,
    RegisterWidgetPayload<'a> {
        name: &'a str,
        icon: &'a str,
        template: &'a str,
        editTemplate: &'a str,
    }
);
