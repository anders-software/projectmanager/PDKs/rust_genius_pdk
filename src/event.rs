use serde::Deserialize;
use serde_json::Value;

#[derive(Deserialize)]
pub struct RawEvent {
    pub name: String,
    arg: Value,
}

impl RawEvent {
    pub fn cast_to<T>(self) -> Result<T, extism_pdk::Error>
    where
        for<'de> T: Deserialize<'de>,
    {
        let t = serde_json::from_value(self.arg)?;
        Ok(t)
    }
}

pub trait Event {
    const IDENTIFIER: &'static str;
}

#[macro_export]
macro_rules! match_event {
    ($input:expr; $(
        $event:ty as $var:ident => $handler:block,
    )+) => {
        match $input.name.as_str() {
            $(
                <$event as $crate::event::Event>::IDENTIFIER => {
                    match $input.cast_to::<$event>() {
                        Ok($var) => $handler,
                        Err(e) => Err(e),
                    }
                },
            )+
            _ => Ok(()), // when we didn't match to an event, we are still Ok
        }
    };
}
